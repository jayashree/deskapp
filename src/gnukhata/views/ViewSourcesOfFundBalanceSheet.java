package gnukhata.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import gnukhata.globals;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class ViewSourcesOfFundBalanceSheet extends Composite {

	/**
	 * @param args
	 */
	
	Table tbLiaBalanceSheet;
	
	
	TableItem headerRow;
	
	TableColumn lia_accname;
	TableColumn lia_tolamt;
	TableColumn lia_amt;
	
	Label lbllia_accname;
	Label lbllia_tolamt;
	Label lbllia_amt;
	Button btnBacktoBalanceSheet;
	Button btnPrint;
	
	TableEditor lia_accnameEditor;
	TableEditor lia_tolamtEditor;
	TableEditor lia_amtEditor;
	
	ArrayList<Button> accounts = new ArrayList<Button>();
	String endDateParam = ""; 
	NumberFormat nf;
	Vector<Object> printsources = new Vector<Object>();
	static Display display;
	String strOrgType;
	ODPackage sheetstream;
	public ViewSourcesOfFundBalanceSheet(Composite parent, int style,String endDate, Object[] result, Object[] profitloss){
		super(parent, style);
		
		strOrgType = globals.session[4].toString();
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		
		Label lblheadline=new Label(this, SWT.NONE);
		String strdate="";
		endDateParam = endDate;
		lblheadline.setText("\t\tStatement of Sources and Applications of Fund");
		//lblheadline.setText("\nFrom "+" " + globals.session[2]+" To "+strdate);
		lblheadline.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(22);
		layout.left = new FormAttachment(28);
		lblheadline.setLayoutData(layout);
		

		Label lblheadline2=new Label(this, SWT.NONE);
		strdate=endDate.substring(8)+" - "+endDate.substring(5,7)+" - "+endDate.substring(0, 4);
		endDateParam = endDate;
		lblheadline2.setText(" " + globals.session[2]+ " To " +strdate);
		lblheadline2.setFont(new Font(display, "Times New Roman", 12, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline,2);
		layout.left = new FormAttachment(35);
		lblheadline2.setLayoutData(layout);
		
		
		tbLiaBalanceSheet= new Table(this, SWT.MULTI|SWT.BORDER|SWT.LINE_SOLID|SWT.FULL_SELECTION);
		tbLiaBalanceSheet.setLinesVisible (true);
		tbLiaBalanceSheet.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblheadline2,10);
		layout.left = new FormAttachment(10);
		layout.right = new FormAttachment(80);
		layout.bottom = new FormAttachment(91);
		tbLiaBalanceSheet.setLayoutData(layout);
		
		btnBacktoBalanceSheet =new Button(this,SWT.PUSH);
		btnBacktoBalanceSheet.setText("&Back To Balance Sheet");
		btnBacktoBalanceSheet.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tbLiaBalanceSheet,20);
		layout.left=new FormAttachment(25);
		btnBacktoBalanceSheet.setLayoutData(layout);
		btnBacktoBalanceSheet.setFocus();
		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tbLiaBalanceSheet,20);
		layout.left=new FormAttachment(50);
		btnPrint.setLayoutData(layout);
		
		
	this.makeaccessible(tbLiaBalanceSheet);
	this.getAccessible();
	//this.pack();
	this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
    this.setReport(result,profitloss);
    /* try {
    	 sheetstream= ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/SourcesAndApplicationOfFunds.ots"),"SourcesAndApplicationOfFunds");
		
	} catch (IOException e) {
		// TODO: handle exception
		e.printStackTrace();
	}
    */
	}
	private void setReport(Object[] result,Object[] profitloss)
	{
		String[] columns;
		String[] orgname;
		orgname=new String[]{ globals.session[1].toString()};
		
		lbllia_accname = new Label(tbLiaBalanceSheet,SWT.BORDER|SWT.CENTER);
		lbllia_accname.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		
			lbllia_accname.setText("");
			columns=new String[]{"CAPITAL AND LIABILITIES","Amount","Amount"};
		
		
		
		lbllia_amt = new Label(tbLiaBalanceSheet, SWT.BORDER|SWT.CENTER);
		lbllia_amt.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lbllia_amt.setText("");
		
		lbllia_tolamt = new Label(tbLiaBalanceSheet,SWT.BORDER|SWT.CENTER);
		lbllia_tolamt.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		lbllia_tolamt.setText("");
		
		final int tblliabal = tbLiaBalanceSheet.getClientArea().width;
		tbLiaBalanceSheet.addListener(SWT.MeasureItem, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub

				lia_accname.setWidth(50*tblliabal/100);
				lia_amt.setWidth(20*tblliabal/100);
				lia_tolamt.setWidth(20*tblliabal/100);
				event.height = 10;
			}
		});
		
		btnBacktoBalanceSheet.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnBacktoBalanceSheet.getParent().getParent();
				btnBacktoBalanceSheet.getParent().dispose();
					
				viewBalanceSheet bs = new viewBalanceSheet(grandParent,SWT.NONE);
				bs.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
				
		
		headerRow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		TableItem[] items = tbLiaBalanceSheet.getItems();
		lia_accname = new TableColumn(tbLiaBalanceSheet, SWT.BORDER);
		lia_amt = new TableColumn(tbLiaBalanceSheet, SWT.RIGHT);
		lia_tolamt = new TableColumn(tbLiaBalanceSheet, SWT.RIGHT);
		

		lia_accnameEditor = new TableEditor(tbLiaBalanceSheet);
		lia_accnameEditor.grabHorizontal = true;
		lia_accnameEditor.setEditor(lbllia_accname,items[0],0);
		
		lia_amtEditor = new TableEditor(tbLiaBalanceSheet);
		lia_amtEditor.grabHorizontal = true;
		lia_amtEditor.setEditor(lbllia_amt,items[0],1);
		
		lia_tolamtEditor = new TableEditor(tbLiaBalanceSheet);
		lia_tolamtEditor.grabHorizontal = true;
		lia_tolamtEditor.setEditor(lbllia_tolamt,items[0],2);
		
		lia_accname.pack();
		lia_amt.pack();
		lia_tolamt.pack();
		
		nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
		String rowFlag = "";
		Integer rows=0;
		Double pnlDr = 0.00;
		
		Integer netflag = profitloss.length - 4;
		Integer netTotalIndex = profitloss.length - 3;
		//Integer netTotal = profitloss.length - 3;
		Integer ballength=result.length - 13; 
		
		Integer tol_loanlia = result.length - 5;
		Integer tol_currlia = result.length - 6;
		Integer tol_fixesAsset = result.length -8;
		Integer tol_miscellaneous = result.length - 7;
		Integer tol_investment = result.length - 11;
		Integer tol_loansasset = result.length - 10;
		Integer tol_currentasset = result.length -9;
		Integer assSrNo = Integer.parseInt(result[result.length-13].toString());
		Integer liaSrNo = Integer.parseInt(result[result.length-12].toString());
		Integer lialength = result.length - 1;
		Integer asslength = result.length - 2;
		
		Double 	tol_capital = Double.parseDouble(result[result.length - 4].toString());
		Double  tol_reserves =Double.parseDouble(result[result.length - 3].toString());
		Double tol_capitalAndreserves = tol_capital + tol_reserves;
		
		Double TotalDr =Double.parseDouble(result[lialength].toString());
		Double TotalCr = Double.parseDouble(result[asslength].toString());
		Double netTotal = Double.parseDouble(profitloss[netTotalIndex].toString());
		pnlDr = netTotal + TotalDr;
		Double pnlCr = Float.parseFloat(profitloss[netTotalIndex].toString()) + TotalCr;
		Double difamount = 0.00;
		Double balancingTotal = 0.00;
		
		
		if (assSrNo > liaSrNo)
		{
			rowFlag = "liabilities";
			rows = assSrNo - liaSrNo;
		}
		
		
		if(assSrNo < liaSrNo)
		{
			rowFlag = "asset";
			rows = liaSrNo - assSrNo;
		}

		if(profitloss[netflag].equals("netProfit"))
		{
			if(TotalDr > pnlCr)
			{
				difamount = TotalDr - pnlCr;
			}
			else
			{
				difamount = pnlCr - TotalDr;
			}
		}
		else
		{
			if(TotalCr>pnlDr)
			{
				difamount = TotalCr - pnlDr;
			}
			else
			{
				difamount = pnlDr - TotalCr;
			}
		}
		
		int grpcode1=0;
		int grpcode12=0;
		int grpcode11=0;
		int grpcode3=0;
		int grpcode6=0;
		int grpcode2=0;
		int grpcode10=0;
		int grpcode9=0;
		int grpcode13=0;
		
		// For loop to get the length of the accounts of the respective groups from the result 
		btnPrint.setData("printorgname",orgname);
		btnPrint.setData("printcolumns",columns);
		for(int cnt =0; cnt < ballength; cnt++)
		{
			Object[] len = (Object[]) result[cnt];
			if(len[1].equals(1))
			{
				 grpcode1++;
			}
			if(len[1].equals(12))
			{
				 grpcode12++;
			}
			if(len[1].equals(11))
			{
				 grpcode11++;
			}
			if(len[1].equals(3))
			{
				 grpcode3++;
			}
			if(len[1].equals(6))
			{
				grpcode6++;
			
			}
			if(len[1].equals(9))
			{
				grpcode9++;
			
			}if(len[1].equals(2))
			{
				grpcode2++;
			
			}
			if(len[1].equals(10))
			{
				grpcode10++;
			
			}
			if(len[1].equals(13))
			{
				grpcode13++;
			
			}
		}
		

		grpcode3 = grpcode1 + grpcode3;
		grpcode11 = grpcode3 + grpcode11;
		grpcode12 = grpcode11 + grpcode12;
		grpcode6 = grpcode12 + grpcode6;
		grpcode2 = grpcode6 +  grpcode2;
		grpcode10 = grpcode2 + grpcode10;
		grpcode9 = grpcode10 + grpcode9;
		grpcode13 = grpcode9 + grpcode13;
		
		
		/* Code to display the accounts of group Capital,Reserves,Loans,Current Liabilities in the
		Liabilities side of the Balance Sheet */
		
		TableItem source = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		source.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		source.setText(0, "SOURCES");
		
		TableItem lia_acc=new TableItem(tbLiaBalanceSheet, SWT.NONE);
		if(strOrgType == "profit making")
		{
			
			lia_acc.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
			lia_acc.setText(0, "                                CAPITAL AND LIABILITIES                               ");
			lia_acc.setText(1, "AMOUNT                               ");
			lia_acc.setText(2, "AMOUNT                               ");

			columns=new String[]{"CAPITAL AND LIABILITIES","Amount","Amount"};
		}
		else
		{
			lia_acc.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
			lia_acc.setText(0, "                               CORPUS AND LIABILITIES                               ");
			lia_acc.setText(1, "AMOUNT                               ");
			lia_acc.setText(2, "AMOUNT                               ");
			columns=new String[]{"CORPUS AND LIABILITIES","Amount","Amount"};
		}
		

		
		TableItem balrow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		balrow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		if(strOrgType.equals("ngo"))
		{
			balrow.setText(0, "CORPUS");
			balrow.setText(2, tol_capitalAndreserves.toString());
			Object[] printableRow = new Object[]{"CORPUS","",tol_capitalAndreserves};
			printsources.add(printableRow);
		}
		if(strOrgType.equals("profit making"))
		{
			balrow.setText(0, "CAPITAL");
			balrow.setText(2, tol_capitalAndreserves.toString());
			Object[] printableRow = new Object[]{"CAPITAL","",tol_capitalAndreserves};
			printsources.add(printableRow);
		}
		
		for(int rowcounter =0; rowcounter < grpcode1; rowcounter ++)
		{
			TableItem balrow1 = new TableItem(tbLiaBalanceSheet, SWT.NONE);	
			balrow1.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] baldata = (Object[]) result[rowcounter];
			
			if(baldata[1].equals(1))
			{
				balrow1.setText(0, "            " + baldata[2].toString());
				balrow1.setText(1, "            " + baldata[3].toString());
				Object[] printableRow = new Object[]{baldata[2],baldata[3]};
				printsources.add(printableRow);

			}
			
			
		}
		
		/*TableItem reserve_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		reserve_row.setText(0,"RESERVES");
		reserve_row.setText(2,result[tol_reserves].toString());
		*/
		
		for(int rowcounter =grpcode11; rowcounter < grpcode12; rowcounter ++)
		{
			TableItem reserve1_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			reserve1_row.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] baldata2 = (Object[]) result[rowcounter];
			
			if(baldata2[1].equals(12))
			{
				reserve1_row.setText(0, "            " + baldata2[2].toString());
				reserve1_row.setText(1, "            " + baldata2[3].toString());
				Object[] printableRow = new Object[]{baldata2[2],baldata2[3]};
				printsources.add(printableRow);

			}
		}


		TableItem loan_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		loan_row.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		loan_row.setText(0,"LOANS");
		loan_row.setText(2,result[tol_loanlia].toString());
		Object[] printableRow = new Object[]{"LOANS","",result[tol_loanlia]};
		printsources.add(printableRow);

		for(int rowcounter =grpcode3; rowcounter < grpcode11; rowcounter ++)
		{
			TableItem loan1_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			loan1_row.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] baldata3 = (Object[]) result[rowcounter];
			
			if(baldata3[1].equals(11))
			{
				loan1_row.setText(0, "            " + baldata3[2].toString());
				loan1_row.setText(1, "            " + baldata3[3].toString());
				printableRow = new Object[]{baldata3[2],baldata3[3]};
				printsources.add(printableRow);

			}
		}
		
		TableItem lia_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		lia_row.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		lia_row.setText(0,"CURRENT LIABILITIES");
		lia_row.setText(2,result[tol_currlia].toString());
		printableRow = new Object[]{"CURRENT LIABILITIES","",result[tol_currlia]};
		printsources.add(printableRow);
		for(int rowcounter =grpcode1; rowcounter < grpcode3; rowcounter ++)
		{
			TableItem lia1_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			lia1_row.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] baldata4 = (Object[]) result[rowcounter];
			
			if(baldata4[1].equals(3))
			{
				lia1_row.setText(0, "            " + baldata4[2].toString());
				lia1_row.setText(1, "            " + baldata4[3].toString());
				printableRow = new Object[]{baldata4[2],baldata4[3]};
				printsources.add(printableRow);
			}
		}
		
		if(profitloss[netflag].equals("netProfit"))
		{
			TableItem netProfitRow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			netProfitRow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			if(strOrgType.equals("profit making"))
			{
				netProfitRow.setText(0,"NET PROFIT");
				printableRow = new Object[]{"NET PROFIT"};
				printsources.add(printableRow);
			}
			else
			{
				netProfitRow.setText(0,"NET SURPLUS");
				printableRow = new Object[]{"NET SURPLUS"};
				printsources.add(printableRow);
			}
			netProfitRow.setText(2,profitloss[netTotalIndex].toString());
			printableRow = new Object[]{"","",profitloss[netTotalIndex]};
			printsources.add(printableRow);
		}
		
		
		if(profitloss[netflag].equals("netLoss"))
		{
			TableItem liaTotalTableItem = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			liaTotalTableItem.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
			liaTotalTableItem.setText(0,"TOTAL");
			printableRow = new Object[]{"TOTAL"};
			printsources.add(printableRow);

			if(difamount != 0.00)
			{
				liaTotalTableItem.setText(2,nf.format(TotalCr));
				printableRow = new Object[]{"","",nf.format(TotalCr)};
				printsources.add(printableRow);

			}
			else
			{
				liaTotalTableItem.setText(2,nf.format(TotalCr));
				printableRow = new Object[]{"","",nf.format(TotalCr)};
				printsources.add(printableRow);

			}
			
			
			if(pnlDr > TotalCr)
			{
				if(difamount != 0.00)
				{	
				TableItem diffbal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
				diffbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
				diffbal.setText(0, "Difference In Opening Balance");
				diffbal.setText(2,nf.format(difamount));
				printableRow = new Object[]{"Difference In Opening Balance","",nf.format(difamount)};
				printsources.add(printableRow);

				balancingTotal = difamount + TotalCr;
				System.out.println("bal total" + balancingTotal);
				TableItem balancingbal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
				balancingbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
				balancingbal.setText(0,"TOTAL");
				balancingbal.setText(2,nf.format(balancingTotal));
				printableRow = new Object[]{"TOTAL","",nf.format(balancingTotal)};
				printsources.add(printableRow);
				}

			}
		

			
			/*TableItem blankrow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			blankrow.setText(0,"");
			blankrow.setText(1,"");
			blankrow.setText(2,"");
			*/
		}
		else
		{		
			TableItem liaTotal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			liaTotal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
			liaTotal.setText(0,"TOTAL");
			printableRow = new Object[]{"TOTAL"};
			printsources.add(printableRow);

				if(difamount != 0.00)
				{
					liaTotal.setText(2,nf.format(pnlCr));
					printableRow = new Object[]{"","",nf.format(pnlCr)};
					printsources.add(printableRow);

				}
				else
				{
					liaTotal.setText(2,nf.format(pnlCr));
					printableRow = new Object[]{"","",nf.format(pnlCr)};
					printsources.add(printableRow);

				}
				
				if(TotalDr > pnlCr)
				{
					if(difamount != 0.00)
					{	
					TableItem diffbal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
					diffbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
					diffbal.setText(0, "Difference In Opening Balance");
					diffbal.setText(2,nf.format(difamount));
					printableRow = new Object[]{"Difference In Opening Balance","",nf.format(difamount)};
					printsources.add(printableRow);

					balancingTotal = difamount + pnlCr;
					System.out.println("bal total" + balancingTotal);
					TableItem balancingbal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
					balancingbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
					balancingbal.setText(0,"TOTAL");
					balancingbal.setText(2,nf.format(balancingTotal));
					printableRow = new Object[]{"TOTAL","",nf.format(balancingTotal)};
					printsources.add(printableRow);

				}
			
				}
				
				/*TableItem blankrow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
				blankrow.setText(0,"");
				blankrow.setText(1,"");
				blankrow.setText(2,"");*/
		}
		
		//Code to show the Difference in Opening Balance in Liabilities side
	TableItem blankrow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		blankrow.setText(0,"");
		blankrow.setText(1,"");
		blankrow.setText(2,"");
		printableRow = new Object[]{"","",""};
		printsources.add(printableRow);

		
		/* Code to display the accounts of groups Fixed Assets,Investment,Current Assets,Assets Loans,
		Miscellaneous Expenses on the Assets side of the Balance Sheet */
		
		TableItem app = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		app.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		app.setText(0, "APPLICATIONS");
		
		printableRow = new Object[]{"APPLICATIONS","",""};
		printsources.add(printableRow);

		
		TableItem asset = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		asset.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
		asset.setText(0, "                                PROPERTY AND ASSETS                                ");
		asset.setText(1, "AMOUNT                               ");
		asset.setText(2, "AMOUNT                               ");
		printableRow = new Object[]{"PROPERTY AND ASSETS","AMOUNT","AMOUNT"};
		printsources.add(printableRow);

		
		TableItem assestbalrow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		assestbalrow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		assestbalrow.setText(0,"FIXED ASSETS");
		assestbalrow.setText(2, result[tol_fixesAsset].toString());
		printableRow = new Object[]{"FIXED ASSETS","",result[tol_fixesAsset].toString()};
		printsources.add(printableRow);

		
		for(int rowcounter =grpcode12; rowcounter < grpcode6; rowcounter ++)
		{
			TableItem assetrow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			assetrow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] Asset_baldata = (Object[]) result[rowcounter];
				
			if(Asset_baldata[1].equals(6))
			{
				assetrow.setText(0, "            " + Asset_baldata[2].toString());
				assetrow.setText(1, "            " + Asset_baldata[3].toString());
				printableRow = new Object[]{Asset_baldata[2],Asset_baldata[2]};
				printsources.add(printableRow);


			}
		}

		TableItem invest_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		invest_row.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		invest_row.setText(0,"INVESTMENTS");
		invest_row.setText(2, result[tol_investment].toString());
		printableRow = new Object[]{"INVESTMENTS",result[tol_investment]};
		printsources.add(printableRow);

		for(int rowcounter =grpcode2; rowcounter < grpcode9; rowcounter ++)
		{
			TableItem invest_row1 = new TableItem(tbLiaBalanceSheet, SWT.NONE);	
			invest_row1.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] invest_baldata = (Object[]) result[rowcounter];
				
			if(invest_baldata[1].equals(9))
			{
				invest_row1.setText(0, "            " + invest_baldata[2].toString());
				invest_row1.setText(1, "            " + invest_baldata[3].toString());
				printableRow = new Object[]{ invest_baldata[2], invest_baldata[3]};
				printsources.add(printableRow);

			}
		}
		
		TableItem currasset_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		currasset_row.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		currasset_row.setText(0,"CURRENT ASSETS");
		currasset_row.setText(2, result[tol_currentasset].toString());
		printableRow = new Object[]{"CURRENT ASSETS","",result[tol_currentasset]};
		printsources.add(printableRow);
		
		for(int rowcounter =grpcode6; rowcounter < grpcode2; rowcounter ++)
		{
			TableItem currasset_row1 = new TableItem(tbLiaBalanceSheet, SWT.NONE);	
			currasset_row1.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] currasset_baldata = (Object[]) result[rowcounter];
				
			if(currasset_baldata[1].equals(2))
			{
				currasset_row1.setText(0, "            " + currasset_baldata[2].toString());
				currasset_row1.setText(1, "            " + currasset_baldata[3].toString());
				printableRow = new Object[]{currasset_baldata[2],currasset_baldata[3] };
				printsources.add(printableRow);
			}
		}
		
		TableItem loansAssets = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		loansAssets.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		loansAssets.setText(0,"ASSET LOANS");
		loansAssets.setText(2,result[tol_loansasset].toString());
		printableRow = new Object[]{"ASSET LOANS","",result[tol_loansasset]};
		printsources.add(printableRow);
		for(int rowcounter = grpcode2; rowcounter < grpcode10;rowcounter++)
		{
			TableItem loansAssetsrow = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			loansAssetsrow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] loansAssetsdata = (Object[]) result[rowcounter];
			if(loansAssetsdata[1].equals(10))
			{

				loansAssetsrow.setText(0, "            " + loansAssetsdata[2].toString());
				loansAssetsrow.setText(1, "            " + loansAssetsdata[3].toString());}
			printableRow = new Object[]{loansAssetsdata[2],loansAssetsdata[3]};
			printsources.add(printableRow);
		}
		
		TableItem misexp_row = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		misexp_row.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		misexp_row.setText(0,"MISCELLANEOUS EXPENSE");
		misexp_row.setText(2, result[tol_miscellaneous].toString());
		printableRow = new Object[]{"MISCELLANOUS EXPENSE","",result[tol_miscellaneous]};
		printsources.add(printableRow);
		
		
		for(int rowcounter =grpcode9; rowcounter < grpcode13; rowcounter ++)
		{
			TableItem misexp_row1 = new TableItem(tbLiaBalanceSheet, SWT.NONE);	
			misexp_row1.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
			Object[] misexp_baldata = (Object[]) result[rowcounter];
				
			if(misexp_baldata[1].equals(13))
			{
				misexp_row1.setText(0, "            " + misexp_baldata[2].toString());
				misexp_row1.setText(1, "            " + misexp_baldata[3].toString());
				printableRow = new Object[]{misexp_baldata[2],misexp_baldata[3]};
				printsources.add(printableRow);

			}
		}
		
		TableItem netLoss = new TableItem(tbLiaBalanceSheet, SWT.NONE);
		netLoss.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
		if(profitloss[netflag].equals("netLoss"))
		{
			if(strOrgType.equals("profit making"))
			{
				netLoss.setText(0,"NET LOSS");
				printableRow = new Object[]{"NET LOSS"};
				printsources.add(printableRow);
			}
			else
			{
				netLoss.setText(0,"NET DEFICIT");
				printableRow = new Object[]{"NET DEFICIT"};
				printsources.add(printableRow);
			}
			netLoss.setText(2,profitloss[netTotalIndex].toString());
			printableRow = new Object[]{"","",profitloss[netTotalIndex]};
			printsources.add(printableRow);
		}
		else
		{
			netLoss.setText(0,"");
			netLoss.setText(1,"");
			netLoss.setText(2,"");
			printableRow = new Object[]{"","",""};
			printsources.add(printableRow);
		}
		
		if(profitloss[netflag].equals("netLoss"))
		{
			TableItem liaTotalTableItem = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			liaTotalTableItem.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
			liaTotalTableItem.setText(0,"TOTAL");
			printableRow = new Object[]{"TOTAL"};
			printsources.add(printableRow);
			System.out.println("Total" + difamount);
			if(difamount != 0.00)
			{
				liaTotalTableItem.setText(2,nf.format(pnlDr));
				printableRow = new Object[]{"","",nf.format(pnlDr)};
				printsources.add(printableRow);
			}
			else
			{
				liaTotalTableItem.setText(2,nf.format(pnlDr));
				printableRow = new Object[]{"","",nf.format(pnlDr)};
				printsources.add(printableRow);
			}
			
			if(pnlDr < TotalCr)
			{
				if(difamount != 0.00)
				{	

				TableItem Assetdiffbal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
				Assetdiffbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
				Assetdiffbal.setText(0, "Difference In Opening balance");
				Assetdiffbal.setText(2,nf.format(difamount));
				printableRow = new Object[]{"","","","Difference In Opening balance","",nf.format(difamount)};
				printsources.add(printableRow);

				balancingTotal = difamount + pnlDr;
				System.out.println("bal total" + balancingTotal);
				TableItem balancingbal_ass = new TableItem(tbLiaBalanceSheet, SWT.NONE);
				balancingbal_ass.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
				balancingbal_ass.setText(0,"TOTAL");
				balancingbal_ass.setText(2,nf.format(balancingTotal));
				printableRow = new Object[]{"","","","TOTAL","",nf.format(balancingTotal)};
				printsources.add(printableRow);

			}
			}
			
			
		}
		else
		{
			TableItem totalCrTableItem = new TableItem(tbLiaBalanceSheet, SWT.NONE);
			totalCrTableItem.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
			totalCrTableItem.setText(0,"TOTAL");
			printableRow = new Object[]{"TOTAL"};
			printsources.add(printableRow);
			if(difamount != 0.00)
			{
				totalCrTableItem.setText(2,nf.format(TotalDr));
				printableRow = new Object[]{"","",nf.format(TotalDr)};
				printsources.add(printableRow);
			}
			else {

				totalCrTableItem.setText(2,nf.format(TotalDr));
				printableRow = new Object[]{"","",nf.format(TotalDr)};
				printsources.add(printableRow);
			}
			
			if(TotalDr < pnlCr)
			{
				if(difamount != 0.00)
				{	

				TableItem Assetdiffbal = new TableItem(tbLiaBalanceSheet, SWT.NONE);
				Assetdiffbal.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
				Assetdiffbal.setText(0, "Difference In Opening balance");
				Assetdiffbal.setText(2,nf.format(difamount));
				printableRow = new Object[]{"","","","Difference In Opening balance","",nf.format(difamount)};
				printsources.add(printableRow);

				balancingTotal = difamount + TotalDr;
				System.out.println("bal total" + balancingTotal);
				TableItem balancingbal_ass = new TableItem(tbLiaBalanceSheet, SWT.NONE);
				balancingbal_ass.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
				balancingbal_ass.setText(0,"TOTAL");
				balancingbal_ass.setText(2,nf.format(balancingTotal));
				printableRow = new Object[]{"","","","TOTAL","",nf.format(balancingTotal)};
				printsources.add(printableRow);

			}
			}

		}
		
				btnPrint.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						// TODO Auto-generated method stub
						//super.widgetSelected(arg0);
						String[] orgname=(String[])btnPrint.getData("printorgname");
						String[] columns = (String[]) btnPrint.getData("printcolumns");
						Object[][] orgdata =new Object[printsources.size()][orgname.length ];
						Object[][] finalData =new Object[printsources.size()][columns.length];
						for(int counter = 0; counter < printsources.size(); counter ++ )
						{
							Object[] printRow = (Object[]) printsources.get(counter);												
							finalData[counter] = printRow;
						}
							
						//printLedgerData.copyInto(finalData);
						
							TableModel model = new DefaultTableModel(finalData,columns);
							/*try {
								final File sources = new File("/tmp/gnukhata/Report_Output/SourcseAndApplicationOfFunds" );
								//final File sourcestemplate = new File("Report_Templates/SourcesAndApplicationOfFunds.ots");
								//SpreadSheet.createEmpty(model).saveAs(sources);
								final Sheet sourcebalSheet = sheetstream.getSpreadSheet().getFirstSheet();
								sourcebalSheet.ensureRowCount(100000);
								sourcebalSheet.getCellAt(0,0).setValue(globals.session[1].toString());
								sourcebalSheet.getCellAt(0,1).setValue("Statement Of Sources And Application Of Funds");
								sourcebalSheet.getCellAt(0,2).setValue(globals.session[2]+" To "+globals.session[2]);
								System.out.println(printsources.size());
								for(int rowcounter=0; rowcounter< printsources.size(); rowcounter++)
								{
									Object[] printrow = (Object[]) printsources.get(rowcounter);
									sourcebalSheet.getCellAt(0,rowcounter+4).setValue(printrow[0].toString());
									System.out.println(printrow[1]);
									//sourcebalSheet.getCellAt(1,rowcounter+4).setValue(printrow[1].toString());
									//sourcebalSheet.getCellAt(2,rowcounter+4).setValue(printrow[2].toString());
									//System.out.println(printrow[1].toString());
								}
									OOUtils.open(sourcebalSheet.getSpreadSheet().saveAs(sources));

								//OOUtils.open(sources);
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
*/					}
				});
	
				
	}
public void makeaccessible(Control c) {
	/*
	 * getAccessible() method is the method of class Control which is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
	c.getAccessible();
}

protected void checkSubclass() {
	// this is blank method so will disable the check that prevents
	// subclassing of shells.
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}