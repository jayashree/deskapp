package gnukhata.views;

/*
 * @authors
 * Amit Chougule <acamit333@gmail.com>,
 * Girish Joshi <girish946@gmail.com>
 * Vinay khedekar < vinay.itengg@gmail.com>
*/


import java.text.NumberFormat;

import gnukhata.globals;
import gnukhata.controllers.StartupController;
import gnukhata.controllers.accountController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;

import com.sun.org.apache.xalan.internal.xsltc.dom.AbsoluteIterator;
/*
 * this class is the loginform for the gnukhata.
 */
public class CreateAccountForm extends Shell
{
	boolean verifyFlag = false;
	boolean onceEdited = false;
	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	String searchText = "";
	long searchTexttimeout = 0;
	public static Label lblsavemsg;
	Label lblRegiNo;
	Label lblCreateAcc;
	Label lblcrdrblnc;
	Label lblGroupName;
	Combo dropdownGroupName;
	Label lblSubGroupName;
	Combo dropdownSubGroupName;
	Label lblSubNewGroupName;
	Text txtSubNewGroupName1;
	Label lblAccountName;
	Text txtAccountName;
	Label lblAccountCode;
	Text txtAccountCode;
	Label lblOpeningBalance;
	Text txtOpeningBalance ;
	Label lblTotalDrOpeningBalance;
	Text txtTotalDrOpeningBalance;
	Label lblTotalCrOpeningBalance;
	Text txtTotalCrOpeningBalance;
	Label lblDiffInOpeningBalance ;
	Text txtDiffInOpeningBalance;
	Button btnSave;
	Button btnFinish;
	Button btnClose;
	String suggestedAccountCode = "null";

	NumberFormat nf = NumberFormat.getInstance();
	Object[] queryParams = new Object[8];
	boolean accountExists = false;
	public CreateAccountForm() {
		super(display);
		
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		nf.setGroupingUsed(false);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		this.setText("Create Account ");
		
		Label lblHeadline = new Label(this,SWT.None);
		lblHeadline.setFont(new Font(display, "Times New Roman", 13, SWT.BOLD));
		lblHeadline.setText("GNUKhata a Free and Open Source Accounting Software");
		FormData layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(3);
		layout.right = new FormAttachment(49);
		layout.bottom = new FormAttachment(5);
		lblHeadline.setLayoutData(layout);
		
		Label lblLogo = new Label(this, SWT.None);
		//Image img = new Image(display,"finallogo.png");
		lblLogo.setImage(globals.logo);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(100);
		layout.bottom = new FormAttachment(12);
		lblLogo.setLayoutData(layout);
		
		Label lblPageName = new Label(this,SWT.None);
		lblPageName.setText("Create Account");
		lblPageName.setFont(new Font(display, "Times New Roman", 15, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(15,0);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(35);
		layout.bottom = new FormAttachment(20);
		lblPageName.setLayoutData(layout);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+" For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout.top = new FormAttachment(6);
		layout.left = new FormAttachment(3);
		layout.right = new FormAttachment(69);
		layout.bottom = new FormAttachment(16);
		lblOrgDetails.setLayoutData(layout);
	    
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblOrgDetails , 1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(96);
		layout.bottom = new FormAttachment(22);
		lblLine.setLayoutData(layout);
		
		/*lblCreateAcc = new Label(this, SWT.NONE);
		lblCreateAcc.setText("Create Account");
		lblCreateAcc.setFont(new Font(display,"Times New Romen",15,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(27);
		layout.left = new FormAttachment(15);
		layout.right = new FormAttachment(40);
		layout.bottom = new FormAttachment(32);
		lblCreateAcc.setLayoutData(layout);*/
		
		lblGroupName = new Label(this, SWT.NONE);
		lblGroupName.setText("&Group Name :");
		lblGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(35);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(50);
		layout.bottom = new FormAttachment(38);
		lblGroupName.setLayoutData(layout);
		
		lblsavemsg = new Label(this, SWT.NONE);
		lblsavemsg.setFont(new Font(display, "Time New Roman", 10, SWT.BOLD|SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(95);
		layout.left = new FormAttachment(btnClose,10);
		layout.right = new FormAttachment(69);
		lblsavemsg.setLayoutData(layout);

		
		dropdownGroupName = new Combo(this, SWT.READ_ONLY);
		dropdownGroupName.setFont(new Font(display,"Times New Romen",9,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(35);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(39);
		dropdownGroupName.setLayoutData(layout);
		
		lblSubGroupName = new Label(this, SWT.NONE);
		lblSubGroupName.setText("S&ub-Group Name :");
		lblSubGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(40);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(51);
		layout.bottom = new FormAttachment(45);
		lblSubGroupName.setLayoutData(layout);
		
		dropdownSubGroupName = new Combo(this, SWT.READ_ONLY);
		dropdownSubGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(40);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(44);
		dropdownSubGroupName.setLayoutData(layout);
		
		lblSubNewGroupName = new Label(this, SWT.NONE);
		lblSubNewGroupName.setText("Create new &subgroup :");
		lblSubNewGroupName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(45);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(49);
		layout.bottom = new FormAttachment(49);
		lblSubNewGroupName.setLayoutData(layout);
		lblSubNewGroupName.setVisible(false);
		
		txtSubNewGroupName1 = new Text(this, SWT.BORDER);
		txtSubNewGroupName1.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(45);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(49);
		txtSubNewGroupName1.setLayoutData(layout);
		txtSubNewGroupName1.setVisible(false);
		
		lblAccountName = new Label(this, SWT.NONE);
		lblAccountName.setText("A&ccount Name :");
		lblAccountName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(51);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(49);
		layout.bottom = new FormAttachment(55);
		lblAccountName.setLayoutData(layout);
		
		txtAccountName = new Text(this, SWT.BORDER);
		txtAccountName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(51);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(55);
		txtAccountName.setLayoutData(layout);
		
		Label lblAccountCode = new Label(this, SWT.NONE);
		lblAccountCode.setText("Account Code :");
		lblAccountCode.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(57);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(45);
		layout.bottom = new FormAttachment(61);
		lblAccountCode.setLayoutData(layout);
		
		txtAccountCode = new Text(this, SWT.BORDER);
		txtAccountCode.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(57);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(61);
		txtAccountCode.setLayoutData(layout);
		
		lblOpeningBalance = new Label(this, SWT.NONE);
		lblOpeningBalance.setText("&Opening Balance :");
		lblOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(63);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(49);
		//layout.bottom = new FormAttachment(66);
		lblOpeningBalance.setLayoutData(layout);
		
		txtOpeningBalance = new Text(this, SWT.RIGHT|SWT.BORDER);
		txtOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(63);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(67);
		txtOpeningBalance.setText("0.00");
		txtOpeningBalance.setLayoutData(layout);
		
		lblTotalDrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalDrOpeningBalance.setText("Total Debit Opening Balance (₹):");
		lblTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(69);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(57);
		layout.bottom = new FormAttachment(73);
		lblTotalDrOpeningBalance.setLayoutData(layout);
		
		txtTotalDrOpeningBalance = new Text(this, SWT.RIGHT|SWT.BORDER);
		txtTotalDrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(69);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(73);
		txtTotalDrOpeningBalance.setEnabled(true);
		txtTotalDrOpeningBalance.setLayoutData(layout);
		
		lblTotalCrOpeningBalance = new Label(this, SWT.NONE);
		lblTotalCrOpeningBalance.setText("Total Credit Opening Balance (₹):");
		lblTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(75);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(57);
		layout.bottom = new FormAttachment(80);
		lblTotalCrOpeningBalance.setLayoutData(layout);
		
		txtTotalCrOpeningBalance = new Text(this, SWT.RIGHT| SWT.BORDER);
		txtTotalCrOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(75);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(79);
		txtTotalCrOpeningBalance.setLayoutData(layout);
		txtTotalCrOpeningBalance.setEnabled(true);
		
		lblDiffInOpeningBalance = new Label(this, SWT.NONE);
		lblDiffInOpeningBalance.setText("Difference in Opening Balance (₹):");
		lblDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(81);
		layout.left = new FormAttachment(20);
		layout.right = new FormAttachment(57);
		layout.bottom = new FormAttachment(87);
		lblDiffInOpeningBalance.setLayoutData(layout);
		
		txtDiffInOpeningBalance = new Text(this, SWT.RIGHT|SWT.BORDER);
		txtDiffInOpeningBalance.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(81);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(67);
		layout.bottom = new FormAttachment(85);
		txtDiffInOpeningBalance.setLayoutData(layout);
		txtDiffInOpeningBalance.setEnabled(true);
		
		lblcrdrblnc = new Label(this,SWT.NONE);
		lblcrdrblnc.setFont(new Font(display,"Times New Romen",10,SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(82);
		layout.left = new FormAttachment(68);
		layout.right = new FormAttachment(78);
		layout.bottom = new FormAttachment(85);
		lblcrdrblnc.setLayoutData(layout);
		
		btnSave = new Button(this,SWT.PUSH);
		btnSave.setText("&Save");
		btnSave.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(90);
		layout.left = new FormAttachment(30);
		layout.right = new FormAttachment(35);
		layout.bottom = new FormAttachment(95);
		btnSave.setLayoutData(layout);
		
		btnFinish = new Button(this,SWT.PUSH);
		btnFinish.setText("F&inish");
		btnFinish.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(90);
		layout.left = new FormAttachment(40);
		layout.right = new FormAttachment(50);
		layout.bottom = new FormAttachment(95);
		btnFinish.setLayoutData(layout);
		
		btnClose = new Button(this,SWT.PUSH);
		btnClose.setText("C&lose Organization");
		btnClose.setFont(new Font(display, "Times New Roman", 10, SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(90);
		layout.left = new FormAttachment(60);
		layout.right = new FormAttachment(80);
		layout.bottom = new FormAttachment(95);
		btnClose.setLayoutData(layout);


		//txtOpeningBalance.setText(nf.format(Double.parseDouble("0.00")));
		
		
		String[] allgroups = gnukhata.controllers.accountController.getAllGroups();
		dropdownGroupName.add("Please select");
		dropdownGroupName.select(0);
		dropdownGroupName.setFocus();
		for (int i = 0; i < allgroups.length; i++ )
		{
			dropdownGroupName.add(allgroups[i]);
			
		}
		if(globals.session[5].toString().equals("automatic") )
		{
			txtAccountCode.setVisible(false);
			lblAccountCode.setVisible(false);
		}
		this.getAccessible();
		this.setEvents();
		this.pack();
		this.makeaccessible(this);
		this.open();
		this.showView();
		}
	
	private void setEvents()
	{
		this.txtOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				
				verifyFlag = true;
			}
			public void focusLost(FocusEvent arg0){
				verifyFlag=false;

				try {
					txtOpeningBalance.setText(nf.format(Double.parseDouble(txtOpeningBalance.getText())));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		this.dropdownGroupName.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				String selectedGroup = dropdownGroupName.getItem(dropdownGroupName.getSelectionIndex());
				String[] subGroups = gnukhata.controllers.accountController.getSubGroups(selectedGroup);
				queryParams[0] = selectedGroup;
				dropdownSubGroupName.removeAll();
				if(selectedGroup.equals("Capital"))
				{
					suggestedAccountCode = "CP";
				}
				if(selectedGroup.equals("Current Asset"))
				{
					suggestedAccountCode = "CA";
				}
				if(selectedGroup.equals("Fixed Assets"))
				{
					suggestedAccountCode = "FA";
				}
				if(selectedGroup.equals("Direct Income"))
				{
					suggestedAccountCode = "DI";
				}
				if(selectedGroup.equals("Indirect Income"))
				{
					suggestedAccountCode = "II";
				}
				if(selectedGroup.equals("Current Liability"))
				{
					suggestedAccountCode = "CL";
				}
				if(selectedGroup.equals("Loans(Asset)"))
				{
					suggestedAccountCode = "LA";
				}
				if(selectedGroup.equals("Loans(Liability)"))
				{
					suggestedAccountCode = "LL";
				}
				if(selectedGroup.equals("Miscellaneous Expenses(Asset)"))
				{
					suggestedAccountCode = "ME";
				}
				if(selectedGroup.equals("Direct Expense"))
				{
					suggestedAccountCode = "DE";
				}
				if(selectedGroup.equals("Investment"))
				{
					suggestedAccountCode = "IV";
				}
				if(selectedGroup.equals("Reserves"))
				{
					suggestedAccountCode = "RS";
				}
				if(selectedGroup.equals("Indirect Expense"))
				{
					suggestedAccountCode = "IE";
				}
				
				
				dropdownSubGroupName.add("Please select");
				dropdownSubGroupName.select(0);
				if(dropdownGroupName.getText().trim().equals("Please select"))
				{
					lblOpeningBalance.setText("&Opening Balance :");
				}
				if(selectedGroup.equals("Current Asset") || selectedGroup.equals("Fixed Assets") || selectedGroup.equals("Investment") || selectedGroup.equals("Loans(Asset)"))
				{
					lblOpeningBalance.setText("Debit &Opening Bal : ");
					//txtOpeningBalance.setText("0.00");
				}
				if (selectedGroup.equals("Capital") || selectedGroup.equals("Corpus") || selectedGroup.equals("Current Liability") || selectedGroup.equals("Loans(Liability)") || selectedGroup.equals("Miscellaneous Expenses(Asset)") || selectedGroup.equals("Reserves") )
				{
					lblOpeningBalance.setText("Credit &Opening Bal : ");
					//txtOpeningBalance.setText("0.00");
					//txtOpeningBalance.setText(nf.format(Double.parseDouble("0.00")));
				}
				if( selectedGroup.equals("Direct Income") || selectedGroup.equals("Indirect Income") || selectedGroup.equals("Direct Expense") || selectedGroup.equals("Indirect Expense") )
				{
					lblOpeningBalance.setEnabled(false);
					
					txtOpeningBalance.setEnabled(false);
					//dropdownSubGroupName.add("No Sub-Group");
				}
				else
				{
					lblOpeningBalance.setEnabled(true);
					txtOpeningBalance.setEnabled(true);
					//txtOpeningBalance.setText(nf.format(Double.parseDouble("0.00")));
					
					//txtOpeningBalance.setText(nf.format(Double.parseDouble(txtOpeningBalance.getText())));
					
				}

				for (int i = 0; i < subGroups.length; i++ )
					dropdownSubGroupName.add(subGroups[i]);
				
			}
			
		});
		this.dropdownGroupName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					if(dropdownGroupName.getSelectionIndex() == 0)
					{
						MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msg.setMessage("Please select Group name");
						msg.open();
						dropdownGroupName.setFocus();
					}
					else if(dropdownGroupName.getSelectionIndex() != 0)
					{
					dropdownGroupName.notifyListeners(SWT.Selection, new Event());
					dropdownSubGroupName.setFocus();
					}
				}
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(e.character);
				searchTexttimeout = now + 500;
				
				for(int i = 0; i < dropdownGroupName.getItemCount(); i++ )
				{
					if(dropdownGroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
						dropdownGroupName.select(i);
						break;
					}
				}
			}
		});
		this.txtAccountName.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					if(txtAccountCode.isVisible())
					{
					txtAccountCode.setFocus();
					}
					else
					{
						if(txtOpeningBalance.isEnabled())
						{
							txtOpeningBalance.setFocus();
						//	txtOpeningBalance.setText(nf.format(Double.parseDouble("0.00")));
						}
						else
						{
							btnSave.setFocus();
							btnSave.notifyListeners(SWT.Selection ,new Event() );
						}
					}
				}
			if(e.keyCode== SWT.ARROW_UP)
				{
				   if(dropdownSubGroupName.getSelectionIndex()==1)
					{
						dropdownSubGroupName.setFocus();
					}
						
					
					else
					{
						txtSubNewGroupName1.setFocus();
					}
					
				}
			}
			
		});
		this.txtSubNewGroupName1.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					txtAccountName.setFocus();
				}
				if(e.keyCode== SWT.ARROW_UP)
				{
					dropdownSubGroupName.setFocus();
				}
			}
			
		});
		this.txtAccountCode.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					txtOpeningBalance.setFocus();
				}
				if(e.keyCode== SWT.ARROW_UP)
				{
					txtAccountName.setFocus();
				}
			}
			
		});
		this.txtOpeningBalance.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode== SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					btnSave.notifyListeners(SWT.Selection ,new Event() );
				}
				if(e.keyCode== SWT.ARROW_UP)
				{
					txtAccountName.setFocus();
				}
			}
			
		});
		
		this.btnSave.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				
				if(e.keyCode== SWT.ARROW_RIGHT)
				{
					btnFinish.setFocus();
				}
			}
			
		});
		
		this.btnFinish.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				
				if(e.keyCode== SWT.ARROW_RIGHT)
				{
					btnClose.setFocus();
				}
				if(e.keyCode== SWT.ARROW_LEFT)
				{
					btnSave.setFocus();
				}
			}
			
		});
		
		this.btnClose.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				
				
				if(e.keyCode== SWT.ARROW_LEFT)
				{
					btnFinish.setFocus();
				}
			}
			
		});
		this.dropdownSubGroupName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				{
					if(dropdownSubGroupName.getSelectionIndex() < 0)
					{
						MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msg.setMessage("Please select Subgroup name");
						msg.open();
						dropdownSubGroupName.setFocus();
					}
					else if(dropdownSubGroupName.getSelectionIndex() >= 0)
					{
						if(txtSubNewGroupName1.isVisible())
						{
							txtSubNewGroupName1.setFocus();		
						}
						else if(!txtSubNewGroupName1.isVisible())
						{
							txtAccountName.setFocus();
						}
					}	
					
			}		
			
				long now = System.currentTimeMillis();
				if (now > searchTexttimeout){
			         searchText = "";
			      }
				searchText += Character.toLowerCase(e.character);
				searchTexttimeout = now + 1000;					
				for(int i = 0; i < dropdownSubGroupName.getItemCount(); i++ )
				{
					if(dropdownSubGroupName.getItem(i).toLowerCase().startsWith(searchText ) ){
						//arg0.doit= false;
						dropdownSubGroupName.select(i);
						break;
					}
				}			
				if(e.keyCode== SWT.ARROW_UP )
				{
					if(dropdownSubGroupName.getSelectionIndex() == 0)
					{
						dropdownGroupName.setFocus();
					}
				}
			}
		});
		
		
		dropdownSubGroupName.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				if(dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex()).equals("Create New Sub-Group"))
				{
					lblSubNewGroupName.setVisible(true);
					txtSubNewGroupName1.setVisible(true);
					queryParams[1] = "Create New Sub-Group";
					
				}
				else
				{
					lblSubNewGroupName.setVisible(false);
					txtSubNewGroupName1.setVisible(false);
					queryParams[1] = dropdownSubGroupName.getItem(dropdownSubGroupName.getSelectionIndex());
					queryParams[2] = ""; 

				}
			}
		});
		this.txtAccountName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				if(!txtAccountName.getText().trim().equals(""))
				{
					if(! onceEdited )
					{
					txtAccountName.setText(Character.toUpperCase(txtAccountName.getText().charAt(0)) + txtAccountName.getText().substring(1) );
					onceEdited = true;
					}
					String result = accountController.accountExists(txtAccountName.getText());
					if (Integer.valueOf(result) == 1)
					{
						MessageBox	 msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
						msg.setMessage("the account name you entered already exists, please choose another name.");
						msg.open();
						txtAccountName.setText("");
						txtAccountName.setFocus();
						accountExists = true;
						return;
					}
					else
					{
						accountExists = false;
						queryParams[3] = txtAccountName.getText();
					}
					if(globals.session[5].toString().equals("manually") )
					{
						queryParams[4] = "manually";
						suggestedAccountCode = suggestedAccountCode + txtAccountName.getText().substring(0,1);
						suggestedAccountCode = accountController.getSuggestedAccountCode(suggestedAccountCode);
						txtAccountCode.setText(suggestedAccountCode);
						queryParams[7] = suggestedAccountCode;
					}
					else
					{
						queryParams[7] = "";
					}
				}
			}
		});
		
		dropdownGroupName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				if(! lblsavemsg.getText().equals(""))
				{
					Display.getCurrent().asyncExec(new Runnable(){
						public void run()
						{
							long now = System.currentTimeMillis();
							long lblTimeOUt = 0;
							while(lblTimeOUt < (now + 2000))
							{
								lblTimeOUt = System.currentTimeMillis();
							}
					
							lblsavemsg.setText("");

						}
				});

					
				}
			}
			
			@Override
			public void focusLost(FocusEvent arg0)
			{
				if(dropdownGroupName.getSelectionIndex()==1 || dropdownGroupName.getSelectionIndex()==13 || dropdownGroupName.getSelectionIndex()==12 || dropdownGroupName.getSelectionIndex()==10 || dropdownGroupName.getSelectionIndex()==4 || dropdownGroupName.getSelectionIndex()==5 || dropdownGroupName.getSelectionIndex()==7 || dropdownGroupName.getSelectionIndex()==8)
				{
					if(dropdownSubGroupName.getItem(0).contentEquals("Please select"))
					{
					dropdownSubGroupName.remove(0);
					dropdownSubGroupName.select(0);
					dropdownSubGroupName.notifyListeners(SWT.Selection,new Event());
					}
				}
			
			}
			
		});

		
		this.btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				queryParams[4] = globals.session[5];
				//queryParams[7] = suggestedAccountCode;
				//super.widgetSelected(arg0);
				queryParams[7] = "";
				if(accountExists)
				{
					txtAccountName.setFocus();
					return;
				}
				suggestedAccountCode	 = "";
				if(dropdownGroupName.getSelectionIndex()== 0 )
				{
					MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
					errMessage.setText("Error!");
					errMessage.setMessage("Please select a group for this account");
					errMessage.open();
					dropdownGroupName.setFocus();
					return;
				}
				if(dropdownSubGroupName.getText().trim().equals("Please select" ))
				{
					MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
					errMessage.setText("Error!");
					errMessage.setMessage("Please select a Sub-group or NO Sub-Group for this account");
					errMessage.open();
					dropdownSubGroupName.setFocus();
					return;
				}
				if(txtAccountName.getText().trim().equals("") )
				{
					MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
					errMessage.setText("Error!");
					errMessage.setMessage("Please enter an Account Name");
					errMessage.open();
					txtAccountName.setFocus();

					return;
				}
				
				if ( dropdownSubGroupName.getText().equals("Create New Sub-Group") && txtSubNewGroupName1.getText().trim().equals(""))
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK | SWT.ERROR);
					msg.setText("Error");
					msg.setMessage("Please enter an New Sub-Group Name");
					msg.open();
					txtSubNewGroupName1.setFocus();
					return;
				}
				if(txtSubNewGroupName1.getVisible())
				{
					queryParams[2] = txtSubNewGroupName1.getText();
					
				}
				if(txtOpeningBalance.getText().trim().equals("")|| txtOpeningBalance.getEnabled()== false )
				{
					queryParams[5] = "0.00";
					queryParams[6] = "0.00";
					
				}
				else
				{
					

					queryParams[5] = nf.format(Double.parseDouble((txtOpeningBalance.getText())));
					queryParams[6] = nf.format(Double.parseDouble((txtOpeningBalance.getText())));
				}
				
				/*for (int i=0; i < queryParams.length; i++ )
				{
					MessageBox msg = new MessageBox(new Shell(), SWT.OK );
					msg.setMessage(queryParams[i].toString());
					msg.open();
				}*/
				
				if(accountController.setAccount(queryParams))
				{
					lblsavemsg.setText("Account " + txtAccountName.getText() + "  added successfully");
					lblsavemsg.setVisible(true);
					
					/*MessageBox successMsg = new MessageBox(new Shell(),SWT.OK| SWT.ICON_INFORMATION);
					successMsg.setText("success");
					successMsg.setMessage("Account "+ txtAccountName.getText() + " added successfully" );
					successMsg.open();
					dispose();
					StartupController.showCreateAccount();
					*/dropdownGroupName.select(0);
					dropdownSubGroupName.select(0);
					txtAccountName.setText("");
					txtAccountCode.setText("");
					lblOpeningBalance.setText("&Opening Balance :");
					txtOpeningBalance.setText("0.00");
					txtTotalDrOpeningBalance.setText("0.00");
					txtTotalCrOpeningBalance.setText("0.00");
					txtSubNewGroupName1.setText("");
					txtSubNewGroupName1.setVisible(false);
					lblSubNewGroupName.setVisible(false);
					dropdownGroupName.setFocus();
					onceEdited = false;
				}
				else
				{
					MessageBox msg = new MessageBox(new Shell(), SWT.OK | SWT.ERROR);
					msg.setMessage("the account could not be saved, plese try again.");
					msg.open();
				}
				Double totalDrOpeningBalance = 0.00;
				Double totalCrOpeningBalance = 0.00;
				Double diffBalance = 0.00;
				
				totalCrOpeningBalance= accountController.getTotalCr();
				totalDrOpeningBalance = accountController.getTotalDr();
				//nf.format(Double.parseDouble((txtOpeningBalance.getText())));
				txtTotalCrOpeningBalance.setText(nf.format(Double.parseDouble(totalCrOpeningBalance.toString())));
				txtTotalDrOpeningBalance.setText( nf.format(Double.parseDouble(totalDrOpeningBalance.toString())));
				diffBalance =   totalCrOpeningBalance - totalDrOpeningBalance;
				if(diffBalance < 0 )
				{
					diffBalance = Math.abs(diffBalance);
				}
				Double val=	diffBalance;
				txtDiffInOpeningBalance.setText(nf.format( Double.parseDouble(val.toString())));
				if(totalDrOpeningBalance>totalCrOpeningBalance)
				{
					lblcrdrblnc.setText("Dr");
				}
				else if (totalCrOpeningBalance>totalDrOpeningBalance) 
				{
					lblcrdrblnc.setText("Cr");
				}
				else
				{
					lblcrdrblnc.setText("");
				}
						
				
				
			}
		});
		this.txtSubNewGroupName1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
			// TODO Auto-generated method stub
				//super.focusLost(arg0);
				String result = accountController.subgroupExists(txtSubNewGroupName1.getText());
				MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
				if( Integer.valueOf(result) == 1)
				{
					msg.setMessage("The subgroup name you entered already exists");
					msg.open();
					txtSubNewGroupName1.setText("");
					txtSubNewGroupName1.setFocus();
				}
				}
		});
		
		this.btnFinish.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				dispose();
				gnukhata.controllers.StartupController.showLoginForm();
				
		
			}
		});
		
		this.btnClose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				 btnClose.getParent().dispose();
				startupForm sf=new startupForm();
				
		
			}
		});
		
		txtOpeningBalance.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent arg0) {
				// TODO Auto-generated method stub
				if(verifyFlag== false)
				{
					arg0.doit= true;
					return;
				}
				if(arg0.keyCode==46)
				{
					return;
				}
				if(arg0.keyCode==45)
				{
					return;
				}
				switch (arg0.keyCode) {
	            case SWT.BS:           // Backspace
	            case SWT.DEL:          // Delete
	            case SWT.HOME:         // Home
	            case SWT.END:          // End
	            case SWT.ARROW_LEFT:   // Left arrow
	            case SWT.ARROW_RIGHT:  // Right arrow
	                return;
	        }
				if(arg0.keyCode  == 46)
				{
					return;
				}
	        if (!Character.isDigit(arg0.character))
	        {
	            arg0.doit = false;  // disallow the action
	        }

				

			}
		});

		txtTotalCrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				super.focusGained(arg0);
				btnSave.setFocus();
			}
		});
		txtTotalDrOpeningBalance.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				super.focusGained(arg0);
				btnSave.setFocus();
			}
		});
		txtDiffInOpeningBalance.addFocusListener(new FocusAdapter() {
		@Override
		public void focusGained(FocusEvent arg0) {
			// TODO Auto-generated method stub
			super.focusGained(arg0);
			btnSave.setFocus();
		}
		});
	}
	
	
	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}
	private void showView()
	{
		while(! this.isDisposed())
		{
			if(! this.getDisplay().readAndDispatch())
			{
				this.getDisplay().sleep();
				if ( ! this.getMaximized())
				{
					this.setMaximized(true);
				}
			}
			
		}
		this.dispose();
	}

}